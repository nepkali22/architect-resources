
# Instalación

docker-compose up

# Pruebas unitarias de patrones de diseño:

composer install -o --prefer-dist

./vendor/bin/phpunit --bootstrap vendor/autoload.php -v --debug code 

# Pruebas de test con texto formateado de la prueba
./vendor/bin/phpunit --bootstrap vendor/autoload.php --testdox code/

# Chequeo estandar codigo psr2

./vendor/bin/phpcs -p --standard=PSR2 --ignore=vendor .

# Reparar automáticamente en base al estandar psr2 el código

./vendor/bin/phpcbf -p --standard=PSR2 --ignore=vendor .