#Comunicacion Efectiva

##Tipos de Comunicación
* **Comunicación escrita**:Es el tipo de comunicación más común en las empresas, es usada principalmente 
para compartir el estado de algún problema, actualizaciones del estado o la resolución de un problema
como habilidad del negocio esta es usada para compartir muchos tipos de datos, en el trabajo
son por ejemplo, reportes, emails, contenido web, cartas, manuales de empleado, etc.

* **Comunicacíon verbal**:Ocurre cuando tu intercambias mensajes o información a través del habla, esta puede ser por ejemplo
cara a cara, por teléfono, por video-llamada. Se utiliza en una variedad de situaciones y algo importante de este tipo de 
comunicación es que puede reflejar el nivel de profesionalismo y de conocimiento de un tema tienen los implicados.

* **Comunicación no-verbal**:Este tipo de comunicación involucra el envio y recepción de mensajes no hablados, por ejemplo,
expresiones faciales, lenguaje corporal, postura, y contacto con la mirada, es un reto mayor el detectar estas señales y si se dectectan
pueden generar un gran impacto en cualquier situación, se puede percibir las necesidades y sentimientos, puede ayudar a realizar un proceso
más colaborativo, respetuoso y fluido.

##Code Switching
Se realiza cuando estamos en un entorno profesional, debemos ser conscientes e intencionales con un cambio del código, un
cambio de código se puede utilizar cuando hay un cambio de audiencia o de entorno, es decir, dependiendo de donde te encuentres
 (Ambiente) deberás comunicarte de forma diferente con la audiencia (Code Switching), por ejemplo, si yo estoy con mis abuelos
no me comportaré o expresaré igual que si estuviera con mis amigos.

En el ámbito laboral y profesional debe hacer match el tono y el estilo de comunicación que utilices con la situación, para ello se debe
de tomar en cuenta tres aspectos: *mensaje*, *audiencia* y *urgencia*.

* **Mensaje**: que tal detallado debe ser el mensaje que necesito comunicar y compleja.

* **Audiencia**:cual es el nivel de formalidad apropiado en esa situación y cuales son las restricciones de tiempo.

* **Urgencia**:que tan crítica es la información para poder tomar acciones.

En general, para tener una buena comunicación verbal y escrita adecuada y que sea profesional en el espacio de trabajo deberemos
**dirigirnos a otros de forma respetuosa** y deberemos **evitar palabras informales**.

##Comunicación estructurada
Para generar el mayor impacto en tu comunicación debes verificar la estructura de tu información, esto con el fin de mantener la atencion
de la audiencia, una comunicación efectiva empieza  con una introducción sólida que transmita el contexto del mensaje y la
relevancia e introduce tu recomendación. El apoyo de tus argumentos deben seguir a la construcción de tus argumentos.

##Framework para tus comunicaciones: SCQS
Tu apertura afecta a como tu audiencia recibe tu mensaje. Para abrir un mensaje o un canal de comunicación de forma efectiva se debe
explicar el contexto para que tu comunicación con los demás sea clara y los enganche desde el inicio.

Para ello la estructura SCQS puede ayudarte:
![SCQS](./scqs.png "SCQS")

Imaginemos que tenemos un negocio de computadoras y nos enfrentamos a un nuevo competidor en nuestro mercado, bajo este 
escenario es como aplicariamos SCQS:

* **Situación**: Historicamente hemos lidereado el mercado manteniendo altos estándares para la entera satisfacción
de cliente y mantener su lealtad a nosotros, incrementando nuestro número de nuevos clientes.

* **Complicación**: Ha ingresado al mercado un nuevo competidor a bajo costo, esto ha hecho que perdamos nuestros actuales 
clientes y nuevos.

* **Cuestionamiento**: Como nuestro negocio puede recuperar las ganancias y ofrecer sus productos a un costo más bajo.

* **Solución**: Creo que se puede tener una campaña de lealtad, verificar con nuestros proveedores como bajar el costo de los
productos e insumos que tenemos y mejorar nuestra calidad en el servicio al cliente.

##Top-Down Thinking (TDT)
Es un framework que puede ser aplicado en todos los tipos de comunicacion, email, memo, 
conversaciones informales, presentaciones en Power Point, etc. Ayuda a tener una comunicación efectiva, se debe utilizar la Pirámide
TDT:

* **Introducción narrativa**: identifica la razón de tu comunicación, aqui es donde se explora el porque el tema de interés
debe interesar a la audiencia y explica como se resolverá el problema con una solución propuesta.

* **Puntos claves**: es la sección encargada en explicar de una forma hipotetica la solución propuesta en la introducción narrativa.

* **Datos de apoyo**: Esta sección presenta la información que respalda los puntos explicados como *Puntos clave*.

##Ejemplo Top-Down Thinking
Basándonos en la pirámide Top-Down Thinking mostrado a continuación:

![Top-Down Thinking](./piramide-top-down-thinking.png "Top-Down Thinking")

Se nos presenta el siguiente problema:

*Tenemos una empresa que desarrolla software a medida, hemos sido líderes en el mercado por 5 años, en los últimos meses se ha
catapultado una nueva empresa la cuál genera desarrollos de software a medida con mucho menor costo que el nuestro.*

###Paso 1: Crearemos la introducción narrativa
![SCQS-PASO-1](./scqs-paso-1.png "Paso 1")

Ejemplo: 

*En los ultimos años nuestra empresa ha sido líder en el mercado de desarrollo de software a medida manejando altos estándares de calidad **(S)**.
Desafortunadamente se ha presentado en los últimos meses una nueva empresa de nombre X la cuál hemos encontrado que genera desarrollos
de software igualmente a medida pero a un costo bajo, viendo mermada nuestra cartera de clientes **(C)**, ¿Cómo podemos disminuir
los costos de nuestro desarrollo a medida sin comprometer la calidad que nos caracteriza con el fin de aumentar el número de clientes?**(Q)**.
Los puntos que propongo es: verificar nuestros procesos que actualmente tenemos para realizar una automatización con el fin de 
reducir costos operativos, investigar a la competencia con el fin de verificar cuales son sus procesos de calidad con los nuestros,
finalmente lanzar una campaña de marketing para atracción de nuevos clientes con los precios y procesos de calidad revisados.**(S)***

###Paso 2: Describir los puntos claves
![SCQS-PASO-2](./scqs-paso-2.png "Paso 2")

Ejemplo:

1. Actualmente se ha identificado que los procesos institucionales para el aseguramiento de la calidad de nuestros productos de
software pueden no ser sostenibles económicamente para la empresa por lo que se deben verificar costos.

2. Se pueden automatizar procesos que actualmente se realizan manualmente, esto puede reducir costos operativos.

3. Se deben crear campañas de marketing agresivas para mostrar a nuestros clientes nuestros nuevos precios para atraerlos nuevamente
a nuestra cartera de clientes así como impactar en nuevos clientes.

###Paso 3: Incorporar datos de soporte
![SCQS-PASO-3](./scqs-paso-3.png "Paso 3")

Ejemplo de soporte a un punto clave:

**Punto clave:** "Se pueden automatizar procesos que actualmente se realizan manualmente, esto puede reducir costos operativos."

##Pasos para enganchar a la audiencia
Una vez que apliques TDT ya puedes tener tus ideas estructuradas de una forma clara, pero ahora debes asegurarte que cuando 
envies tu mensaje a alguien o a una audiencia esta idea sea atractiva para ellos, de nada sirve que tu idea este bien estructurada
si al final la audiencia a la que va dirigida no le es de interés o prefieren usar su móvil. 

###Determina como tu mensaje es relevante para tu audiencia
Debes de ver el tipo de público que tienes, mucha audiencia se centra en el **porqué** de tu idea, a ellos solo les interesa
un alto nivel, mientras que otros les interesa saber el detalle fino **Cómo**, **Donde**, **Cuando**, **Qué**, otras personas
se fascinan con ambos, por ejemplo, si hablas acerca de una nueva serie de anime con un grupo otaku probablemente se interesen
más en la sinopsis, en los personajes, probablemente si tienen parecido con algún otro anime, así como el número de episodios
ya que es un público especializado, pero si esta misma información se hace llegar a medios para promocionar el anime, deberá
ser un mensaje claro, conciso y breve destacando los puntos importantes para enganchar a mayor público.

###Cuenta una historia
Por ejemplo, si se esta exponiendo el nuevo celular tactil se puede recurrir a una anecdota de cuando los teléfonos eran llamados
*teléfonos de ladrillo*, tal vez no entiendan conceptos a detalle como las capacidades de los teléfonos así como la capacidad
de renderizado, pero se sentirán con esa historia más conectados con lo que se está exponiendo y así captarás su atención 
por medio de tu anecdota.

###Reforzar lo planteado

Muchas veces necesitas reforzar lo que estás planteando a la audiencia, esto con el fin que no se dispersen y sigan enfocados
en a problemática o soluciones principales, por ejemplo, si estás explicando acerca del aumento de los combustibles fósiles
deberás presentar por medio de TDT más información o estadísticas del aumento del precio para que la audiencia no empiece a
divagar en saber que gasolina es mejor para su vehiculo puesto que esto no es el tema principal que quieres tratar.


##Lenguaje y tono

Cuando generamos un mensaje o queremos transmitir una idea algo importante a considerar es el lenguaje y el tono, el **lenguaje**
se refiere a la forma en que tu construyes un mensaje, y el **tono** se refiere a la actitud o voz en la cual tu mensaje es
enviado a los interesados.

##Técnicas para la generación de mensajes

###Lenguaje formal
Siempre se debe usar un lenguaje formal, sin emplear comentarios informales, eso generará más impacto y formalidad.

###Usar voz activa
Se puede perder la intención del mensaje si se recurre a la voz pasiva, además que la voz activa da confianza y eficiencia tambien
da un sentido de pertenencia del interlocutor por ejemplo: *voy a dar una fiesta mañana*, y su contraparte en voz pasiva: *
la fiesta será dada por mi mañana*.

###Tener un lenguaje directo y específico
Debes ser específico, el lenguaje debe ser claro y conciso, puede ser dificil ser directo cuando hablas de ti mismo pero en 
la práctica puedes usar frases como *He trabajado con este recurso antes* o *Estoy convencido que podremos diseñar un cuestionario
efectivo*.

###Mantenerse enfocados en la solución
Debes evitar no dispersarte, siempre debes mantener el foco en el problema y en la solución, evitando pensar en las complicaciones,
se debe ser un promotor de éxito no un retractor, si algún compañero por ejemplo pide un reporte que debiste de entregar la semana
pasada no te centres en comunicar que debías de hacer mejor enfócate comunicar como lo resolverás.

###Establecer conexión y confianza personal
Establece conexiones con la audiencia mostrando interés en sus necesidades, demostrando simpatía, o estableciendo metas comunes,
así se sentirán más a gusto para apoyarte ya que van hacia la misma dirección.

##Tipos de conflictos inter-personales

###Pseudo-Conflictos
Un Pseudo-conflicto ocurre mientras hay diferencias entre dos o más individuos, probablemente hay pequeñas burlas, un comportamiento
no acorde a la ocasión, si se percibe que lo realizas tu puedes resolverlos reconociendo tu error.

###Conflictos de hecho
Regularmente suceden como resultado de una disputa por la validez o confiabilidad de la información planteada, se puede resolver
utilizando un recurso con el que todos los integrantes esten deacuerdo y que es confiable.

###Conflictos de valor
Es una disputa acerca de la percepción interna y creencias morales. Disputas políticas y éticas son lós más comunes.

###Conflictos políticos
Es un conflicto por un plan, como va encaminada una acción o un comportamiento. En el trabajo los ejemplo más comunes incluyen
no estar deacuerdo de cual es la mejor manera de proceder con los proyectos o la forma de proceder de un compañero de trabajo
ante algunas acciones.

###Conflictos de ego
Es un conflicto en el cual un individuo o grupo de individuos quieren que sus argumentos sean los **ganadores** sobre los de los
demás. Sucede por ejemplo, cuando múltiples personas creen que deberían estar a cargo.

###Meta-conflictos
Es una disputa acerca de la comunicación en un argumento, habla acerca de si un argumento debe tener un argumento o si la forma
en como se conduce el argumento es correcta.

##Fameworks para el manejo de conflictos inter-personales
Se verán a continuación algunas estrategias para el manejo de conflictos, esto con el fin de poder identificar cual tipo
es el que se acomoda más a nuestras necesidades.

###Acomodación
Involucra a grupos o personas que sacrifican sus propias necesidades y deseos en el espíritu de cooperación, sin embargo el ser
demasiado cooperativo puede afectar un proyecto si no se toman en cuenta los objetivos mayores de los equipos.

###Evitación
Se puede resolver un conflicto de forma momentanea creando un espacio entre las dos partes implicadas, en vez de confrontar los problemas
sin importancia prefieren ignorarlos. Esta táctica permite muchas veces pequeños conflictos puedan resolverse solos pero en general
no sirve como estrategia a largo plazo porque causa problemas en el sentir de los implicados y podrá asi afectar la productividad
laboral.

###Colaboración
Intenta eliminar la percepción de los implicados de una mentalidad **ganar-perder** y optan por reforzar una mentalidad **ganar-ganar**,
la colaboración ayuda a los implicados a apoyarse mutuamente para alcanzar sus objetivos a través de la negociación o solucionando
problemas. Lo complicado es alcanzar un concenso y esto a veces conlleva mucho tiempo y esfuerzo.

###Competencia
Es la aproximación menos deseable de **ganar-perder**, donde una de las partes renuncia a sus metas y deseos sin obtener algún
reconocimiento del otro, este deberá ser usado por lo regular en momentos críticos donde se debe tomar una acción, pero debe tomarse
como de último recurso.

###Compromiso
Este tiene totalmente una percepción **perder-perder**, es una táctica cuando las dos partes no alcanzaron sus metas planteadas.
Esta estrategia es apropiada cuando se requieren soluciones temporales que involucran los objetivos de los implicados, posteriormente
se comprometen las partes implicadas a llegar a sus objetivos en un tiempo en específico.

##Resolviendo conflictos en el espacio de trabajo

Muchas veces se tienen problemas en el trabajo, en juntas, en planeaciones, en pequeños malentendidos que deben atenderse, para ello
podemos usar los siguientes puntos a considerar para resolver de una mejor forma los conflictos:

###Priorizar relaciones entre colegas
Principalmente en este punto es que no debes llevar al ámbito personal el conflicto que tienes en ese momento, esto te ayudará
a mantener el foco de tu atención en el problema a resolver y no enfocarte en pequeñeses.

###Recopila información del problema
Debes investigar e indagar más acerca del contexto del conflicto, el porque se dió, investigar con la contraparte el porque
de su postura para empatizar con ellos, entender el porque adquirieron esa postura y esto ayudará a ver el problema desde 
otro ángulo.

###Estar deacuerdo con los hechos
Se debe primero trabajar en conjunto con el otro equipo si se puede para encontrar cuál es la raiz del problema, al encontrar
la raíz del problema se sabrá que se pretende atacar y asi sabrán el camino que se debe tomar para resolverlo.

###Explorar opciones juntos
Si se trabaja en equipo y se establecen criterios se peuden evaluar soluciones potenciales. Soluciones que se
generen de lluvia de ideas puede resolver el conflicto y evaluarlos.

###Llega a la solución
Elije una solución que pueda satisfacer la mayoria de necesidades de los equipos, si no existe una opcion pondera
las opciones existentes para ver cual es la que más acomoda a todos los equipos,si cada parte prioriza sus problemas
podrás realizar un acuerdo **ganar-ganar**.

###Muevete
Una vez que se haya llegado a la solución *dejalo fluir*, es decir, acepta la resolución y no te frustres, sigue adelante.

#Como generar impacto para la comunicación

##El valor de la comunicación inter-personal
El tener la tecnología a nuestra disposición trae como consecuencia el que no podamos comunicarnos de la mejor manera con el 
receptor, por lo que es recomendable la conversación cara a cara ya que esta trae diversas ventajas:

* crea un dialogo más rico que el de texto
* la conversación se torna más natural
* conectas de forma emocional con el receptor
* es mas facil expresar tus ideas

##Factores verbales y no verbales

###Verbales

####Comunicación clara
Mejorar la pronunciación es encontrar un ritmo en tu mensaje y el poder enviar tus palabras de una forma clara y correcta. 
Piensa en como podrías decir un numero de telefono o direccion, si no tienes un ritmo abruptamente se sentirá no conectado 
y confundido el receptor.
Un patrón de comunicación pacífico como siempre permite tener a tu audiencia confortable y dando un buen mensaje a ellos.

####Volumen
Hablar con un volumen sensible cuando proyectes tu mensaje, y se consciente del ruido ambiental.
Podría ser reconfortante para nostros mismos hablar con un volumen bajo, pero esto no es ultimamente una forma efectiva
de que tu audiencia escuche tu mensaje, especialment si tu estas en un cuarto con más de cincuenta personas.

####Vocabulario
Usas las palabras que sirvan para tu mensaje o ellas te obstaculizan? No elabores con lenguaje innecesario o lenguaje complcado 
y sobretodo que esto te seduzca, menos es más. Si tu auto-editas regularmente tus columnicaciones diarias mejorarán.

###No verbales
####Contacto visual
Cuando estes dando un platica, encamina la junta o teniendo una conversación intima, el contacto visual dice mucho del nivel de
interes de la conversación, la gente juzga tu integridad basada en como o que tanto mantienes el contacto visual. Hace que la
persona a la que tu estas hablando se mantenga enfocada.

####Lenguaje Corporal
La comunicación inter-personal quiere decir que tu estas enviando y recibiendo mensajes en tiempo real, Puedes ser inconsistente
si por ejemplo cruzas tus brazos, frunces el ceño o encoges tus hombros puedes enviar de una forma no intencional señales
que pueden mal-interpretarse. cuando escuchas, se abierto e imparcial hasta que tu has recibido el mensaje completo.

##Lenguaje corporal
###Que hacer y que no hacer