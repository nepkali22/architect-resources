<?php

require "../../../../../../vendor/autoload.php";

use DesignPatterns\Creational\Prototype\Solution\Solucion1\UserRepository;

$userRepository = new UserRepository();

$user1 = $userRepository->getUserWithBooks(1);

print_r($user1);

$user2 =$user1->clone();

print_r($user2);

print_r($user1);
