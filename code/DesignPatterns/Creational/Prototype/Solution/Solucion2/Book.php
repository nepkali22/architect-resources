<?php
namespace DesignPatterns\Creational\Prototype\Solution\Solucion2;

class Book
{
    private $name='';
    private $author='';

    public function __construct(string $name, string $author)
    {
        $this->name=$name;
        $this->author=$author;
    }

    public function __clone()
    {
        $this->name .=" cloned";
        $this->author.=" cloned";
    }
}
