<?php

require "../../../../../vendor/autoload.php";
use DesignPatterns\Creational\Builder\Problem\Mysql;
use DesignPatterns\Creational\Builder\Problem\Postgres;

switch ($argv[1]) {
    case "Mysql":
        $mysql = new Mysql();
        $query = $mysql->select(["user","users"], ['id',"=","3"], [3,5]);
        break;

    case "Postgres":
        $postgres = new Postgres();
        $query = $postgres->select(["user","users"], ['id',"=","3"], [3,5]);
        break;
}

echo "query---->{$query}";
