<?php

namespace DesignPatterns\Creational\FactoryMethod\Solution;

class GmailSender extends EmailSender
{

    private $user=null;
    private $password=null;
    private $aliasAccount=null;

    public function __construct(string $user, string $password, string $aliasAccount = null)
    {
        $this->user=$user;
        $this->password=$password;
        $this->aliasAccount = $aliasAccount==null?$this->user:$aliasAccount;
    }

    public function getEmailProvider():EmailConnector
    {
        return new Gmail($this->user, $this->password, $this->aliasAccount);
    }
}
