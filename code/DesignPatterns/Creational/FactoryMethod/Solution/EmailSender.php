<?php
namespace DesignPatterns\Creational\FactoryMethod\Solution;

abstract class EmailSender
{

    abstract public function getEmailProvider():EmailConnector;

    public function send(array $to, string $subject, string $body):void
    {
        $emailConnector = $this->getEmailProvider();
        $emailConnector->configure();
        $emailConnector->addRecepients($to);
        $emailConnector->send($subject, $body);
        $emailConnector->close();
    }
}
