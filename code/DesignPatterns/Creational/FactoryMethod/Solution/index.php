<?php
require "../../../../../vendor/autoload.php";

use DesignPatterns\Creational\FactoryMethod\Solution\GmailSender;
use DesignPatterns\Creational\FactoryMethod\Solution\HotmailSender;
use DesignPatterns\Creational\FactoryMethod\Solution\EmailSender;

function clientCode(EmailSender $emailSender)
{
    $date = date('Y-m-d H:i:s');
    $emailSender->send(['bluedrayco@gmail.com'], "Prueba 1 - {$date}", "esta es una primera prueba de uso de un conector de email");
    $emailSender->send(['bluedrayco@gmail.com'], "Prueba 2 - {$date}", "esta es una segunda prueba de uso de un conector de email");
}


clientCode(new GmailSender("bluedrayco@gmail.com", "asbbfckwdwhsyyxd", "user_gmail"));
clientCode(new HotmailSender("bluedrayco@hotmail.com", "3n0v4!!!", "user_hotmail"));
