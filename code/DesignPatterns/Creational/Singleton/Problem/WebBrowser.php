<?php

require "../../../../../vendor/autoload.php";

use DesignPatterns\Creational\Singleton\Problem\Controller;

$controller = new Controller();
print_r($controller->sendUsersToWS());
