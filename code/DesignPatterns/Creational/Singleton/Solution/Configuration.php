<?php
namespace DesignPatterns\Creational\Singleton\Solution;

class Configuration
{
    private $filename;
    private static $instance = null;

    private function __construct($filename)
    {
        $this->filename = $filename;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance($filename):Configuration
    {
        if (null === static::$instance) {
            static::$instance = new static($filename);
        }

        return static::$instance;
    }


    public function getConfigurationDatabase()
    {
        $config = json_decode(file_get_contents($this->filename), true);
        return $config['database'];
    }

    public function getConfigurationWS()
    {
        $config = json_decode(file_get_contents($this->filename), true);
        return $config['webservice'];
    }
}
