<?php

require "../../../../../vendor/autoload.php";

use DesignPatterns\Creational\Singleton\Solution\Controller;

$controller = new Controller();
print_r($controller->sendUsersToWS());
