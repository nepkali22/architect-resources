<?php

namespace DesignPatterns\Creational\Singleton\Solution\Test;

use DesignPatterns\Creational\Singleton\Solution\Configuration;
use PHPUnit\Framework\TestCase;

class SingletonTest extends TestCase
{
    public function testPruebaUnicaInstancia()
    {
        $firstCall = Configuration::getInstance('configuration.json');
        $secondCall = Configuration::getInstance('configuration.json');

        $this->assertInstanceOf(Configuration::class, $firstCall);
        $this->assertInstanceOf(Configuration::class, $secondCall);
        $this->assertSame($firstCall, $secondCall);
    }
}
