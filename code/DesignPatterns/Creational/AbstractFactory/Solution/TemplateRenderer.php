<?php
namespace DesignPatterns\Creational\AbstractFactory\Solution;

interface TemplateRenderer
{
    public function render(string $templateString, array $arguments = []): string;
}