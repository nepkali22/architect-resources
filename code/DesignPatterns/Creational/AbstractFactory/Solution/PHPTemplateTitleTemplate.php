<?php
namespace DesignPatterns\Creational\AbstractFactory\Solution;

class PHPTemplateTitleTemplate implements TitleTemplate
{
    public function getTemplateString(): string
    {
        return "<h1><?= \$title; ?></h1>";
    }
}
