<?php

require "../../../../../vendor/autoload.php";

use DesignPatterns\Creational\AbstractFactory\Solution\Page;
use DesignPatterns\Creational\AbstractFactory\Solution\PHPTemplateFactory;
use DesignPatterns\Creational\AbstractFactory\Solution\TwigTemplateFactory;

$page = new Page('Sample page', 'This it the body.');

echo "\n----->Testing actual rendering with the PHPTemplate factory:\n";
echo $page->render(new PHPTemplateFactory());


// Uncomment the following if you have Twig installed.

echo "\n\n----->Testing rendering with the Twig factory:\n"; 
echo $page->render(new TwigTemplateFactory());