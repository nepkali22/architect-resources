<?php
namespace DesignPatterns\Creational\AbstractFactory\Solution;

use Twig\Loader\ArrayLoader;
use Twig\Environment;

class TwigRenderer implements TemplateRenderer
{
    public function render(string $templateString, array $arguments = []): string
    {
        $loader = new ArrayLoader([
            'index.html' => $templateString,
        ]);
        $twig = new Environment($loader);
        return $twig->render('index.html', $arguments);
    }
}
