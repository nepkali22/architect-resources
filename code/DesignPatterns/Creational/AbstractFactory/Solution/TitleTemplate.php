<?php
namespace DesignPatterns\Creational\AbstractFactory\Solution;

interface TitleTemplate
{
    public function getTemplateString(): string;
}