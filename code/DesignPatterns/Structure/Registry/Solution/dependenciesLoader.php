<?php

use DesignPatterns\Structure\Registry\Solution\Registry;
use DesignPatterns\Structure\Registry\Solution\Service\AppLogger;
use DesignPatterns\Structure\Registry\Solution\Service\HTTPClient;

Registry::set(Registry::HTTP_CLIENT, new HTTPClient());
Registry::set(Registry::LOGGER, new AppLogger('app'));
