<?php
namespace DesignPatterns\Structure\Registry\Solution;

use DesignPatterns\Structure\Registry\Solution\Service\Service;
use InvalidArgumentException;

abstract class Registry
{
    const LOGGER = 1;
    const HTTP_CLIENT = 2;

    private static $services = [];
    private static $allowedKeys=[
        Registry::LOGGER,
        Registry::HTTP_CLIENT
    ];

    public static function set(int $key, Service $service)
    {
        if (!in_array($key, self::$allowedKeys)) {
            throw new InvalidArgumentException("Invalid key Given");
        }
        self::$services[$key]=$service;
    }

    public static function get(int $key):Service
    {
        if (!in_array($key, self::$allowedKeys)||!isset(self::$services[$key])) {
            throw new InvalidArgumentException("Invalid key Given");
        }
        return self::$services[$key];
    }
}
