<?php 
namespace DesignPatterns\Structure\Proxy\Solution;

interface Downloader
{
    public function download(string $url): string;
}