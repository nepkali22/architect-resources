<?php
namespace DesignPatterns\Structure\Proxy\Problem;

class Downloader
{
    public function __construct()
    {
    }
    
    public function download(string $url):string
    {
        printf("Downloading file...\n");
        return file_get_contents($url);
    }
}
