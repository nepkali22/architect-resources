<?php
namespace DesignPatterns\Structure\Proxy\Problem;

use DesignPatterns\Structure\Proxy\Problem\Downloader;

class CacheDownloader extends Downloader
{
    private $cache=[];

    public function __construct(){
        parent::__construct();
    }

    public function download($url):string{
        if(!array_key_exists($url,$this->cache)){
            printf("The cache does not exist creating...\n");
            $this->cache[$url]=parent::download($url);
        }
        printf("Get the file cached...");
        return $this->cache[$url];
    }
}
