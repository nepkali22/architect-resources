<?php
namespace DesignPatterns\Structure\Bridge\Problem;

class RenderJSON implements Pages
{

    public function getSimplePage(string $title, string $content):string
    {
        return '
        {
            "title": "'.$title.'",
            "text": "'.$content.'"
        }
        ';
    }

    public function getProductPage(int $id, string $title, string $description, string $image, float $price):string
    {
        return '
        {
            "title": "'.$title.'",
            "text": "'.$description.'",
            "img": "'.$image.'",
            "link": {"href": "'.$image.'", "title": "'.$title.'", "price": '.$price.'}
        }    
        ';
    }
}
