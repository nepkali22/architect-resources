<?php

require "../../../../../vendor/autoload.php";
use DesignPatterns\Structure\Bridge\Problem\RenderHTML;
use DesignPatterns\Structure\Bridge\Problem\RenderJSON;

$renderHTML = new RenderHTML();
echo $renderHTML->getSimplePage("Home", "Welcome to our website!");

echo $renderHTML->getProductPage(123, "Star Wars, episode1", "A long time ago in a galaxy far, far away...", "/images/star-wars.jpeg", 39.95);

$renderJSON = new RenderJSON();

echo $renderJSON->getSimplePage("Home", "Welcome to our website!");

echo $renderJSON->getProductPage(123, "Star Wars, episode1", "A long time ago in a galaxy far, far away...", "/images/star-wars.jpeg", 39.95);
