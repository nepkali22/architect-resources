<?php

require "../../../../../vendor/autoload.php";
use DesignPatterns\Structure\Bridge\Solution\HTMLRenderer;
use DesignPatterns\Structure\Bridge\Solution\Page;
use DesignPatterns\Structure\Bridge\Solution\JSONRenderer;
use DesignPatterns\Structure\Bridge\Solution\Product;
use DesignPatterns\Structure\Bridge\Solution\ProductPage;
use DesignPatterns\Structure\Bridge\Solution\SimplePage;

function clientCode(Page $page)
{
    echo $page->view();
}

//creamos los renderers disponibles
$HTMLRenderer = new HTMLRenderer();
$JSONRenderer = new JSONRenderer();

$page = new SimplePage($HTMLRenderer, "Home", "Welcome to our website!");
echo "HTML generado con contenido simple\n";
clientCode($page);
echo "\n\n";

//cambiamos de renderer con la misma información
$page->changeRenderer($JSONRenderer);
echo "Misma información presentada con formato JSON:\n";
clientCode($page);
echo "\n\n";


$product = new Product(
    123,
    "Star Wars, episode1",
    "A long time ago in a galaxy far, far away...",
    "/images/star-wars.jpeg",
    39.95
);

$page = new ProductPage($HTMLRenderer, $product);
echo "Pagina HTML más elaborada para la visualización de un producto:\n";
clientCode($page);
echo "\n\n";

$page->changeRenderer($JSONRenderer);
echo "Misma información presentada en formato JSON:\n";
clientCode($page);
