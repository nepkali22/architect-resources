<?php

namespace DesignPatterns\Structure\Adapter\Solution;

use DesignPatterns\Structure\Adapter\Solution\Notification;

class EmailNotification implements Notification
{
    private $email = null;

    public function __construct(string $username, string $password, array $recipients)
    {
        $this->email = new Email($username, $password);
        $this->email->setRecipients($recipients);
    }

    public function send(string $title, string $message): void
    {
        $this->email->connect();
        $this->email->send($title, $message);
        $this->email->close();
    }
}
