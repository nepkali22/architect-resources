<?php
namespace DesignPatterns\Structure\Adapter\Solution;

interface Notification
{
    public function send(string $title, string $message);
}
