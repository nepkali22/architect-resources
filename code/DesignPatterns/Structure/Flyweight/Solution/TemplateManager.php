<?php

namespace DesignPatterns\Structure\Flyweight\Solution;

class TemplateManager
{
    private $templates=[];

    public function addTerminal($type, $title, $body)
    {
        $templateType = TemplateFactory::getTemplateType($type);
        $this->templates[]= new Template($title, $body, $templateType);
    }

    public function draw()
    {
        foreach ($this->templates as $template) {
            echo $template->draw();
        }
    }
}
