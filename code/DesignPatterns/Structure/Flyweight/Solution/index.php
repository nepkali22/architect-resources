<?php
require "../../../../../vendor/autoload.php";
use DesignPatterns\Structure\Flyweight\Solution\TemplateManager;
use DesignPatterns\Structure\Flyweight\Solution\TemplateFactory;


function convert($size)
{
    $unit=array('B','KB','MB','GB','TB','PB');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

$startMemory = memory_get_usage();
$templateManager = new TemplateManager();

for($x=0;$x<10000;$x++){
    $templateManager->addTerminal(TemplateFactory::EMAIL_TEMPLATE, "Hola mundo", "como estas?");
    $templateManager->addTerminal(TemplateFactory::JSON_TEMPLATE, "Hola mundo", "como estas?");
}

$templateManager->draw();

$finishMemory = memory_get_usage();

echo "the memory usage was: ".convert($finishMemory-$startMemory)."\n";