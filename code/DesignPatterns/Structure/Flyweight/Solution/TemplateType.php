<?php
namespace DesignPatterns\Structure\Flyweight\Solution;

class TemplateType
{
    private $templateName;
    private $titleTemplate;
    private $bodyTemplate;
    private $formTemplate;

    public function __construct($templateName, $formTemplate, $titleTemplate, $bodyTemplate)
    {
        $this->templateName=$templateName;
        $this->formTemplate = $formTemplate;
        $this->titleTemplate=$titleTemplate;
        $this->bodyTemplate = $bodyTemplate;
        $this->file = file_get_contents(dirname(__FILE__)."/agregar.pdf");
    }
    public function draw($title, $body)
    {
        echo "Rendering template: {$this->templateName}\n";
        $formTemplate = str_replace("&&", $this->titleTemplate."\n".$this->bodyTemplate, $this->formTemplate);
        $formTemplate = str_replace("##", $title, $formTemplate)."\n";
        $formTemplate = str_replace("%%", $body, $formTemplate)."\n";
        echo $formTemplate;
    }
}
