<?php
namespace DesignPatterns\Behavior\Chain\Solution2;

class UsernameExists extends Middleware{
    
    public function check(User $user):void{
        $userRepository = new UserRepository();
        if($userRepository->getUserByUsername($user->getUsername())){
            throw new ValidatorException("The username exists on the database");
        }
        parent::check($user);
    }
}