<?php

namespace DesignPatterns\Behavior\Chain\Solution2;

class User
{
    private $username=null;
    private $email=null;
    private $password=null;
    private $middleware =null;

    public function __construct(string $email, string $username, string $password)
    {
        $this->email=$email;
        $this->password = $password;
        $this->username = $username;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getPassword(){
        return $this->password;
    }

    public function getUsername(){
        return $this->username;
    }

    public function setMiddleware(Middleware $middleware){
        $this->middleware = $middleware;
    }
    
    public function validate()
    {
        $this->middleware->check($this);
    }
}
