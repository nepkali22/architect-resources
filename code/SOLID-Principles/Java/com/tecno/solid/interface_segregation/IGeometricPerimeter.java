package com.tecno.solid.interface_segregation;

public interface IGeometricPerimeter{

    public double perimeter();

}