package com.tecno.solid.interface_segregation;

public interface IGeometricArea{

    public double area();

}