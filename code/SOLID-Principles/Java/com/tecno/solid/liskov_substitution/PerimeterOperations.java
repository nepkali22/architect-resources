package com.tecno.solid.liskov_substitution;

import com.tecno.solid.liskov_substitution.IGeometricShape;
import java.util.List;

public class PerimeterOperations{

    public static double sumPerimeters(List<IGeometricShape> shapes){
        double perimeter = 0;
        for(IGeometricShape shape : shapes) {
            perimeter += shape.perimeter();
        }
        return perimeter;
    }
}