package com.tecno.solid.liskov_substitution;

public interface IGeometricShape{
    public double area();
    public double perimeter();
}