package com.tecno.solid.original;

import java.util.List;

public class Rectangle {
    private double height;
    private double width;

    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }

    public static double sumAreas(List<Rectangle> rectangulos) {
        double area = 0;
        for (Rectangle rectangulo : rectangulos) {
            area += rectangulo.getHeight() * rectangulo.getWidth();
        }
        return area;
    }

    public static double sumPerimeters(List<Rectangle> rectangulos) {
        double perimetro = 0;
        for (Rectangle rectangulo : rectangulos) {
            perimetro += 2 * rectangulo.getHeight() + 2 * rectangulo.getWidth();
        }
        return perimetro;
    }

    public double getHeight() {
        return this.height;
    }

    public double getWidth() {
        return this.width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
