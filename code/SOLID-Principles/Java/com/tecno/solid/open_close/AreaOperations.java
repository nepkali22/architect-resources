package com.tecno.solid.open_close;

import com.tecno.solid.open_close.Shape;
import java.util.List;

public class AreaOperations{

    public static double sumAreas(List<Shape> shapes){
        double area = 0;
        for(Shape shape : shapes) {
            area += shape.area();
        }
        return area;
    }
}