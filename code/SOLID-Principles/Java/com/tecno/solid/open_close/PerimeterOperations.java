package com.tecno.solid.open_close;

import com.tecno.solid.open_close.Shape;
import java.util.List;

public class PerimeterOperations{

    public static double sumPerimeters(List<Shape> shapes){
        double perimeter = 0;
        for(Shape shape : shapes) {
            perimeter += shape.perimeter();
        }
        return perimeter;
    }
}