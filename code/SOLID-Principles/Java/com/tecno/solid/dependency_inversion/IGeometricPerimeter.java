package com.tecno.solid.dependency_inversion;

public interface IGeometricPerimeter{

    public double perimeter();

}