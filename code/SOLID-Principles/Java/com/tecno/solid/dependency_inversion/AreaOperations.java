package com.tecno.solid.dependency_inversion;

import com.tecno.solid.dependency_inversion.IGeometricArea;
import java.util.List;

public class AreaOperations{

    public static double sumAreas(List<IGeometricArea> shapes){
        double area = 0;
        for(IGeometricArea shape : shapes) {
            area += shape.area();
        }
        return area;
    }
}