package com.tecno.solid.dependency_inversion;

import com.tecno.solid.dependency_inversion.IGeometricArea;
import com.tecno.solid.dependency_inversion.IGeometricPerimeter;

public interface IGeometricShape extends IGeometricPerimeter,IGeometricArea{

}