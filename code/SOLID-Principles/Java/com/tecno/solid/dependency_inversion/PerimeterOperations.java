package com.tecno.solid.dependency_inversion;

import com.tecno.solid.dependency_inversion.IGeometricPerimeter;
import java.util.List;

public class PerimeterOperations{

    public static double sumPerimeters(List<IGeometricPerimeter> shapes){
        double perimeter = 0;
        for(IGeometricPerimeter shape : shapes) {
            perimeter += shape.perimeter();
        }
        return perimeter;
    }
}