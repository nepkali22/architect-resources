<?php
namespace App\LiskovSubstitution;

class PerimeterOperations
{
    public static function sumPerimeters($shapes):float
    {
        $perimeter = 0;
        foreach ($shapes as $shape) {
            $perimeter += $shape->perimeter();
        }
        return $perimeter;
    }
}
