<?php
namespace App\LiskovSubstitution;

use App\LiskovSubstitution\IGeometricShape;

class Rectangle implements IGeometricShape
{
    private float $height;
    private float $width;

    public  function __construct(float $height, float $width){
        $this->height = $height;
        $this->width = $width;
    }

    public function area():float{
        return $this->width * $this->height;
    }

    public function perimeter():float{
        return 2 * $this->height + 2 * $this->width;
    }
}