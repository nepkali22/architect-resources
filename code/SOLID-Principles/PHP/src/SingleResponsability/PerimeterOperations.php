<?php
namespace App\SingleResponsability;

use App\SingleResponsability\Rectangle;

class PerimeterOperations{

    public static function sumPerimetros(array $rectangulos):float{
        $perimetro = 0;
        foreach($rectangulos as $rect) {
            $perimetro += 2 * $rect->getHeight() + 2 * $rect->getWidth();
        }
        return $perimetro;
    }
}