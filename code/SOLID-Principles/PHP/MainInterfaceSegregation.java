package com.tecno;

import com.tecno.solid.interface_segregation.Rectangle;
import com.tecno.solid.interface_segregation.EquilateralTriangle;
import com.tecno.solid.interface_segregation.IGeometricShape;
import com.tecno.solid.interface_segregation.IGeometricArea;
import com.tecno.solid.interface_segregation.IGeometricPerimeter;
import com.tecno.solid.interface_segregation.AreaOperations;
import com.tecno.solid.interface_segregation.PerimeterOperations;

import java.util.List;
import java.util.ArrayList;

public class MainInterfaceSegregation {
    public static void main(String[] args) {
        List<IGeometricShape> shapes = new ArrayList<IGeometricShape>();

        shapes.add(new Rectangle(10, 5));
        shapes.add(new Rectangle(4, 6));
        shapes.add(new Rectangle(5, 1));
        shapes.add(new Rectangle(8, 9));
        shapes.add(new EquilateralTriangle(9));
        shapes.add(new EquilateralTriangle(12));

        List<IGeometricArea> shapesArea = new ArrayList<>(shapes);
        double sumAreas = AreaOperations.sumAreas(shapesArea);

        List<IGeometricPerimeter> shapesPerimeter = new ArrayList<>(shapes);
        double sumPerimetros = PerimeterOperations.sumPerimeters(shapesPerimeter);

        System.out.println("el Area es: "+sumAreas+" y el perímetro es: "+sumPerimetros);
    }
}