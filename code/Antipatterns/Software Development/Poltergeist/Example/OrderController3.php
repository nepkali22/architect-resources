<?php

class OrderController
{
    public function payOrder($orderId)
    {
        if($_POST['Order']) {
            $order = Order::findOrFail($orderId);

            try {
                $order->makeInvoice(
                    $_POST['Order'], Auth::user()
                );
            } catch(InvoiceCreationException $e) {
                $this->redirectBack(
                    $e->getMessage()
                );
            }

            $this->redirectTo('orders');
        }
    }
}