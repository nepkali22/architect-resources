<?php

class OrderController
{
    public function payOrder($orderId)
    {
        if($_POST['Order']) {
            $invoicePayment = new InvoicePaymentHandler();
            if($invoicePayment->make($_POST['Order'])) {
                $this->redirectTo('orders');
            }
            
            $errors = $invoicePayment->getErrors();
            $this->redirectBack($errors);
        }
    }
}