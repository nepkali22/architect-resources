<?php

require "./vendor/autoload.php";

use Project\App\Example1\Person;

$person1 = new Person("Roberto","Ramirez",20);
$person2 = new Person("Elvia","Rodriguez",33);

$person1->printInfo();
$person2->printInfo();

echo "\n";
$person1->name = "Guadalupe";

$person1->printInfo();
$person2->printInfo();

echo "\n";
$person2->age = 12;

$person1->printInfo();
$person2->printInfo();
