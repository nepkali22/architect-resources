<?php

require "./vendor/autoload.php";

use Project\App\Example2\Person;

Person::setValues("Roberto","Ramirez",20);
Person::printInfo();
Person::setValues("Elvia","Rodriguez",33);
Person::printInfo();

Person::$name="Guadalupe";

Person::printInfo();

echo "\n";
Person::$age = 12;

Person::printInfo();