package com.tecno.project.app.connection;

public class DatabaseConnection {

    private static DatabaseConnection instance;

    private String host="";
    private String username="";
    private String password="";

    private DatabaseConnection(String host, String username, String password){
        this.host=host;
        this.username=username;
        this.password=password;
    } 

    public static DatabaseConnection getInstance() {
        if (DatabaseConnection.instance == null) {
            DatabaseConnection.instance = new DatabaseConnection("localhost", "admin", "123456");
        }
        return DatabaseConnection.instance;
    }


    private void connect(){
        System.out.println("--->Contectando con host:"+this.host+", username:" +this.username+", password:"+ this.password);
    }

    public void execute(String sql){
        this.connect();
        System.out.println("executando: "+sql);
        this.close();
    }

    private void close(){
        System.out.println("--->Cerrando conexion...");
    }
}
