package com.tecno.project;

import com.tecno.project.app.example2.Person;

public class MainStatic {
    public static void main(String[] args) {

        Person.setValues("Roberto","Ramirez",20);
        Person.printInfo();
        Person.setValues("Elvia","Rodriguez",33);
        Person.printInfo();

        Person.name = "Guadalupe";

        Person.printInfo();

        System.out.println();
        Person.age = 12;

        Person.printInfo();
    }
}