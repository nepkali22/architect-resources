<?php

require "vendor/autoload.php";

use Project\Bank\BankClient;

try{
    $bankClient = new BankClient("Jaime Andrade","Hernandez Sanchez",18,0.0);
    $bankClient->saveMoney(300);
    echo $bankClient->printInfo();
    $bankClient->extractMoney(200);
    echo $bankClient->printInfo();
}catch(Exception $ex){
    echo "Error: ".$ex->getMessage();
}