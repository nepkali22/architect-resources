from app.bank.bank_client import BankClient

try:
    bankClient = BankClient("Jaime Andrade","Hernandez Sanchez",18,0.0)
    bankClient.saveMoney(300)
    print(bankClient.printInfo())
    bankClient.extractMoney(200)
    print(bankClient.printInfo())
except Exception as e:
    print("Error: "+str(e))
