package com.tecno.project.bank;

public class BankClient {
    private String name = null;
    private String lastName = null;
    private int age = 0;
    private double accountBalance = 0.0;

    public BankClient(String name, String lastName, int age, double accountBalance) throws Exception {
        this.validateAge(age);
        this.validateAmount(accountBalance);
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.accountBalance = accountBalance;
    }

    public BankClient(String name, String lastName, int age) throws Exception {
        this.validateAge(age);
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.accountBalance = 0.0;
    }

    public String getFullName() {
        return this.name + " " + this.lastName;
    }

    private void sendEmail(String title, String body) {
        System.out.println("send email: " + title + " with text:" + body);
    }

    public void saveMoney(double amount) {
        this.accountBalance = this.accountBalance + amount;
        this.sendEmail("---saving money...", "it was saved " + amount);
        this.sendAccountBalanceToBank(amount);
    }

    public void extractMoney(double amount) throws Exception{
        double finalAmount = this.accountBalance - amount;
        this.validateAmount(finalAmount);
        this.accountBalance = finalAmount;
        this.sendEmail("---extracting money...", "it was extracted " + amount);
        this.sendAccountBalanceToBank(amount);
    }

    private void sendAccountBalanceToBank(double amount){
        System.out.println("Sending amount: "+amount+" to the bank...");
    }

    private void validateAmount(double accountBalance) throws Exception {
        if (accountBalance < 0) {
            throw new Exception("The account balance cannot be less than 0");
        }
    }

    private void validateAge(int age) throws Exception {
        if (age < 18) {
            throw new Exception("The age is less than 18");
        }
    }

    public String printInfo() {
        return "Name: " + this.getFullName() + ", Age: " + this.age + ", Balance: " + this.accountBalance;
    }
}