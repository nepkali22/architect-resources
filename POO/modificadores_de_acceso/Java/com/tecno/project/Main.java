package com.tecno.project;

import com.tecno.project.bank.BankClient;

public class Main{
    public static void main(String[] args) {
        try{
            BankClient bankClient = new BankClient("Jaime Andrade","Hernandez Sanchez",18,0.0);
            bankClient.saveMoney(300);
            System.out.println(bankClient.printInfo());
            bankClient.extractMoney(200);
            System.out.println(bankClient.printInfo());
        }catch(Exception ex){
            System.out.println("Error: "+ex.getMessage());
        }
    }
}