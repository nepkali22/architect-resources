package com.tecno.project.service.parser;

import com.tecno.project.service.parser.UserParser;
import com.tecno.project.model.User;

public class XML implements UserParser{

    public String parse(User user){
        return "<root>\n"
            + "    <name>"+user.getName()+"</name>\n"
            + "    <lastName>"+user.getLastName()+"</lastName>\n"
            + "    <age>"+user.getAge()+"</age>\n"
            +"</root>";
    }
}