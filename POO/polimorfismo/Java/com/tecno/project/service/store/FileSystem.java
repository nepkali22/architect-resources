package com.tecno.project.service.store;

import com.tecno.project.service.store.StoreDriver;

import java.io.File;

import com.tecno.project.model.User;

public class FileSystem implements StoreDriver{
    private String directory;

    public FileSystem(String directory){
        this.directory=directory;
    }
    
    public void open(){
        System.out.println("abriendo carpeta: "+this.directory);
    }

    public void store(User user){
        String json = "{"
            +"\"name\":\""+user.getName()+"\","
            +"\"lastName\":"+user.getLastName()+"\","
            +"\"age\":\""+user.getAge()+"\"}";
        System.out.println("Almacenando user: "+json);
    }

    public void close(){
        System.out.println("cerrando carpeta");
    }
}