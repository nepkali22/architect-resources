package com.tecno.project.business;

import com.tecno.project.business.UserProcess;
import com.tecno.project.service.store.StoreDriver;
import com.tecno.project.model.User;

public class SyncUserBank implements UserProcess
{
    private String url;
    private String token;

    public SyncUserBank(String url, String token){
        this.url = url;
        this.token = token;
    }

    public void execute(User user){
        String json = "{"
            +"\"name\":\""+user.getName()+"\","
            +"\"lastName\":"+user.getLastName()+"\","
            +"\"age\":\""+user.getAge()+"\"}";

        System.out.println("send information to: "+this.url+"/user with token: "+this.token+": "+json);
    }

    public String toString(){
        return "SyncUserBank process.\n";
    }
}
