from app.business.user_process import UserProcess
from app.service.store.store_driver import StoreDriver
from app.model.user import User


class StoreUser(UserProcess):
    __driver: StoreDriver

    def __init__(self, driver: StoreDriver):
        self.__driver = driver

    def execute(self, user: User) -> None:
        print("Executing StoreUser process...")
        self.__driver.open()
        self.__driver.store(user)
        self.__driver.close()

    def __str__(self) -> str:
        return "+++StoreUser process.\n"
