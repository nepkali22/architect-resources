from app.business.user_process import UserProcess
from app.service.parser.user_parser import UserParser
from app.model.user import User

class SendUserQueue (UserProcess):
    __host:str
    __username:str
    __password:str
    __parser:UserParser

    def __init__(self,host:str,username:str,password:str,parser:UserParser):
        self.__host=host
        self.__username=username
        self.__password=password
        self.__parser = parser

    def execute(self,user:User)->None:
        userParsed = self.__parser.parse(user)
        print("Executing SendUserQueue process...")
        self.__open()
        self.__send(userParsed)
        self.__close()

    def __str__(self)->str:
        return "---SendUserQueue process.\n"

    def __open(self)->None:
        print(f"conectando a RabbitMQ -> {self.__host}:{self.__username}@{self.__password}")

    def __send(self,userParsed:str)->None:
        print(f"sending data: {userParsed}")

    def __close(self)->None:
        print("cerrando conexion en RabbitMQ")
