from app.service.store.store_driver import StoreDriver
from app.model.user import User


class S3 (StoreDriver):
    __clientId: str
    __secret: str
    __bucket: str

    def __init__(self, clientId: str, secret: str, bucket: str):
        self.__clientId = clientId
        self.__secret = secret
        self.__bucket = bucket

    def open(self) -> None:
        print(f"abriendo conexion a s3: {self.__clientId}, {self.__secret}")

    def store(self, user: User) -> None:
        json = {
            "name": user.getName(),
            "lastName": user.getLastName(),
            "age": user.getAge()
        }
        print(f"guardando user en el bucket:{self.__bucket}: {json}")

    def close(self) -> None:
        print("cerrando conexion a s3")
