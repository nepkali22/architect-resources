
from app.service.store.store_driver import StoreDriver
from app.model.user import User


class FileSystem (StoreDriver):
    __directory: str

    def __init__(self, directory: str):
        self.__directory = directory

    def open(self) -> None:
        print(f"abriendo carpeta: {self.__directory}")

    def store(self, user: User) -> None:
        json = {
            "name": user.getName(),
            "lastName": user.getLastName(),
            "age": user.getAge()
        }
        print(f"Almacenando user: {json}")

    def close(self) -> None:
        print("cerrando carpeta\n")
