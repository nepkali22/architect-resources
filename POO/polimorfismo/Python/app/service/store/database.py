
from app.service.store.store_driver import StoreDriver
from app.model.user import User

class Database (StoreDriver):
    __host:str
    __username:str
    __password:str

    def __init__(self,host:str,username:str,password:str):
        self.__host=host
        self.__username=username
        self.__password=password

    def open(self)->None:
        print(f"abriendo conexion: {self.__host}:{self.__username}@{self.__password}")

    def store(self,user:User)->None:
        print(f"INSERT INTO USER({user.getName()},{user.getLastName()},{user.getAge()})")

    def close(self)->None:
        print("cerrando conexion de la base de datos")
