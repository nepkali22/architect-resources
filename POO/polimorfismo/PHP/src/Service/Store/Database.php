<?php

namespace Project\Service\Store;
use Project\Service\Store\StoreDriver;
use Project\Model\User;

class Database implements StoreDriver{
    private string $host;
    private string $username;
    private string $password;

    public function __construct(string $host, string $username, string $password){
        $this->host=$host;
        $this->username=$username;
        $this->password=$password;
    }
    public function open():void{
        echo "abriendo conexion: {$this->host}:{$this->username}@{$this->password}\n";
    }
    public function store(User $user):void{
        echo "INSERT INTO USER({$user->getName()},{$user->getLastName()},{$user->getAge()})\n";
    }
    public function close():void{
        echo "cerrando conexion de la base de datos\n";
    }
}