<?php

namespace Project\Business;
use Project\Business\UserProcess;
use Project\Service\Store\StoreDriver;
use Project\Model\User;

class StoreUser implements UserProcess{
    private StoreDriver $driver;

    public function __construct(StoreDriver $driver){
        $this->driver = $driver;
    }

    public function execute(User $user):void{
        echo "Executing StoreUser process...\n";
        $this->driver->open();
        $this->driver->store($user);
        $this->driver->close();
    }

    public function __toString():string{
        return "+++StoreUser process.\n";
    }
}