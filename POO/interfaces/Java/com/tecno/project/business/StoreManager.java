package com.tecno.project.business;

import com.tecno.project.models.User;
import com.tecno.project.repository.UserRepository;

public class StoreManager {
    public static void storeUser(User user, UserRepository userRepository) {
        System.out.println("--->Storing user...");
        userRepository.open();
        userRepository.store(user);
        userRepository.close();
    }
}