package com.tecno.project;

import com.tecno.project.repository.UserRepository;
import com.tecno.project.business.StoreManager;
import com.tecno.project.repository.Database;
import com.tecno.project.repository.S3;
import com.tecno.project.repository.FileSystem;
import com.tecno.project.models.User;

public class Main {
    public static void main(String[] args) {

        User user = new User("Roberto", "Jimenez", 18);

        S3 s3Repository = new S3("321312321", "sdf32423", "MyBucket");
        StoreManager.storeUser(user, s3Repository);

        System.out.println("\n");

        FileSystem fileSystemRepository = new FileSystem("/home/users");
        StoreManager.storeUser(user, fileSystemRepository);

        System.out.println("\n");

        Database databaseRepository = new Database("localhost", "admin", "admin123");
        StoreManager.storeUser(user, databaseRepository);

    }
}