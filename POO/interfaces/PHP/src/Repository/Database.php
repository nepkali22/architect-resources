<?php

namespace Project\Repository;
use Project\Repository\UserRepository;
use Project\Models\User;

class Database implements UserRepository{
    private string $host;
    private string $user;
    private string $password;

    public function __construct(string $host,string $user,string $password){
        $this->host=$host;
        $this->user=$user;
        $this->password=$password;
    }
    public function open():void{
        echo "Opening database connection: {$this->host}:{$this->user}@{$this->password}\n";
    }
    public function store(User $user):void{
        $userElements = [
            "name"=>$user->getName(),
            "lastName"=>$user->getLastName(),
            "age"=>$user->getAge()
        ];
        echo "Storing user in the database {$user->getName()}\n";
        echo "INSERT INTO USER VALUES('{$userElements['name']}','{$userElements['lastName']}',{$userElements['age']})\n";
    }
    public function close():void{
        echo "Closing connection\n";
    }
}