<?php

namespace Project\Business;
use Project\Models\User;
use Project\Repository\UserRepository;

class StoreManager{
    public static function storeUser(User $user,UserRepository $userRepository):void{
        echo "--->Storing user...\n";
        $userRepository->open();
        $userRepository->store($user);
        $userRepository->close();
    }
}