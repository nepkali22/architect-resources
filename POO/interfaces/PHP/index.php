<?php

require "./vendor/autoload.php";

use Project\Repository\UserRepository;
use Project\Business\StoreManager;
use Project\Repository\Database;
use Project\Repository\S3;
use Project\Repository\FileSystem;
use Project\Models\User;


$user = new User("Roberto","Jimenez",18);

$s3Repository = new S3("321312321","sdf32423","MyBucket");
StoreManager::storeUser($user,$s3Repository);

echo "\n\n";

$fileSystemRepository = new FileSystem("/home/users");
StoreManager::storeUser($user,$fileSystemRepository);

echo "\n\n";

$databaseRepository = new Database("localhost","admin","admin123");
StoreManager::storeUser($user,$databaseRepository);


