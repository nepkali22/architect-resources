package com.tecno.project.inventory;

public class Product {
    public String title = null;
    public String content = null;
    public double price = 0.0;

    public Product(String title, String content, double price) {
        this.title = title;
        this.content = content;
        this.price = price;
    }

    public String printProductInfo() {
        return "Title: " + this.title + ", Content: " + this.content + ", Price: " + this.price;
    }
}