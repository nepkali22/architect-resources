package com.tecno.project.login;

import com.tecno.project.inventory.Product;
import java.util.ArrayList;

public class User {
    ArrayList<Product> purchasedProducts = null;
    public Role role = null;
    public String username = null;
    public String password = null;
    
    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.purchasedProducts = new ArrayList<Product>();
        this.role = new Role("client");
    }

    public void purchasedProduct(Product product) {
        this.purchasedProducts.add(product);
    }

    public String printInfo() {
        String information = "The user " + this.username + " with role " + this.role.printRole()
                + " has the following products:\n";
        for (int x = 0; x < this.purchasedProducts.size(); x++) {
            information += "product: " + this.purchasedProducts.get(x).printProductInfo() + "\n";
        }
        return information;
    }
}
