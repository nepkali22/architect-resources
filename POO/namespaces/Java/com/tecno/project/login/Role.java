package com.tecno.project.login;

class Role {
    public String role = "";

    public Role(String role) {
        this.role = role;
    }

    public String printRole() {
        return "Rol:" + this.role;
    }
}