from app.login.role import Role
from app.inventory.product import Product

class User:
    purchasedProducts: dict
    role: Role
    username: str
    password: str

    def __init__(self,username:str,password:str):
        self.username=username
        self.password=password
        self.purchasedProducts = []
        self.role = Role("client")

    def purchasedProduct(self,product: Product)->None:
        self.purchasedProducts.append(product)

    def printInfo(self)->str:
        information =f"The user {self.username} with role {self.role.printRole()} has the following products:\n"
        for product in self.purchasedProducts :
            information=information + f"product: {product.printProductInfo()}\n"
        return information