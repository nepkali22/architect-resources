from app.project.cliente import Cliente


cliente1 = Cliente()
cliente2 = Cliente("Jose","Aldama","Alvarez")
cliente3 = Cliente("Jose","Aldama","Alvarez",20,450.00)
cliente4 = Cliente("Jose","Aldama",None,300.00)
cliente5 = Cliente("Jose","Aldama",None,18)
cliente6 = Cliente(None,None,None,None,500)

print(cliente1)
print(cliente2)
print(cliente3)
print(cliente4)
print(cliente5)
print(cliente6)