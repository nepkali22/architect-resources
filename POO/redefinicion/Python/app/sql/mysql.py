from app.sql.connection import Connection

class MysqlQueryBuilder:

    _connection:Connection=None
    _select:str=None
    _where:str=None
    _limit:str=None

    def __init__(self,connection:Connection):
        self._connection = connection
        self._select=''
        self._where=''
        self._limit=''

    def select(self,table:str, fields:list)->'MysqlQueryBuilder':
        self._select = "SELECT " + ','.join(fields) + " FROM " + table
        return self

    def where(self,field:str, value:str, operator:str = '=')->'MysqlQueryBuilder':
        if self._select == '' :
            raise Exception("WHERE can only be added after SELECT")

        operator = operator.upper()
        value = f"'{value}'" if operator == 'LIKE' or operator == 'ILIKE' else value
        self._where = f" WHERE {field} {operator} {value}"
        return self

    def limit(self,start:int, offset:int)->'MysqlQueryBuilder':
        if self._select == '' :
            raise Exception("LIMIT can only be added after SELECT")
        
        self._limit = " LIMIT " + str(start) + ", " + str(offset)
        return self

    def _getSQL(self)->str:
        sql = self._select
        if self._where != '':
            sql = sql + self._where
        if self._limit != '':
            sql = sql + self._limit
        
        sql = sql + ";"
        return sql

    def execute(self)->None:
        self._connection.connect()
        print(f"---->{self._getSQL()}")
        self._connection.close()
