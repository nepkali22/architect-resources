package com.tecno.project.sql;

import com.tecno.project.sql.Connection;

public class MySQLQueryBuilder {
    protected Connection connection;
    protected String select;
    protected String where;
    protected String limit;

    public MySQLQueryBuilder(Connection connection) {
        this.connection = connection;
        this.select = "";
        this.where = "";
        this.limit = "";
    }

    // a partir de la version 8 de java se puede realizar el String.join
    public MySQLQueryBuilder select(String table, String[] fields) {
        this.select = "SELECT " + String.join(", ", fields) + " FROM " + table;
        return this;
    }

    public MySQLQueryBuilder where(String field, String value, String operator) throws Exception {
        if (this.select == "") {
            throw new Exception("WHERE can only be added after SELECT");
        }
        operator = operator.toUpperCase();
        value = operator == "LIKE" || operator == "ILIKE" ? "'" + value + "'" : value;
        this.where = " WHERE " + field + " " + operator + " " + value;
        return this;
    }

    public MySQLQueryBuilder limit(int start, int offset) throws Exception {
        if (this.select == "") {
            throw new Exception("LIMIT can only be added after SELECT");
        }
        this.limit = " LIMIT " + start + ", " + offset;
        return this;
    }

    protected String getSQL() {
        String sql = this.select;
        if (this.where != "") {
            sql += this.where;
        }
        if (this.limit != "") {
            sql += this.limit;
        }
        sql += ";";
        return sql;
    }

    public void execute() {
        this.connection.connect();
        System.out.println("---->" + this.getSQL());
        this.connection.close();
    }
}
