<?php

require "./vendor/autoload.php";

use Project\SQL\Connection;
use Project\SQL\MySQLQueryBuilder;
use Project\SQL\PostgreSQLQueryBuilder;

try {
    $connection = new Connection('localhost', 'user', 'password');
    $query = new MySQLQueryBuilder($connection);
    $query->select("users", ["name", "email", "password"])
        ->where("age", 30, "<")
        ->limit(10, 20)
        ->execute();

    echo "\n";
    
    $query = new PostgreSQLQueryBuilder($connection);
    $query->select("users", ["name", "email", "password"])
        ->where("age", 30, "<")
        ->limit(10, 20)
        ->execute();
} catch (Exception $ex) {
    echo "Error: "+$ex->getMessage()."\n";
}
