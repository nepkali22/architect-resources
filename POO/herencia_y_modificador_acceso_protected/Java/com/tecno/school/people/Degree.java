package com.tecno.school.people;

public class Degree {
    private String degree;

    public Degree(String degree){
        this.degree = degree;
    }

    public String getDegree(){
        return this.degree;
    }
}