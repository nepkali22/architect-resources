package com.tecno.school.people;

public class Professor {
    private String name;
    private int age;
    protected Degree degree;

    public Professor(String name,int age,Degree degree){
        this.name = name;
        this.age = age;
        this.degree = degree;
    }

    public String getInfo(){
        return "Name: "+this.name+", Age: "+this.age+", Degree: "+this.degree.getDegree();
    }

    protected String getName(){
        return this.name;
    }

    protected int getAge(){
        return this.age;
    }
}
