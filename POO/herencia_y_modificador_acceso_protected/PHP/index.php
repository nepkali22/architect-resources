<?php

require "vendor/autoload.php";

use  Project\People\Degree;
use Project\People\ResearcherProfessor;


$degree = new Degree("Computer systems engineer");
$researcher = new ResearcherProfessor("Jose Jorge Velazquez Vega", 21, $degree, "Hibrid Computers", 20000);
echo $researcher->getAllInfo()."\n";
