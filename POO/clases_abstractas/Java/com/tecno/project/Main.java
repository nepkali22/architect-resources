package com.tecno.project;

import com.tecno.project.email.Gmail;
import com.tecno.project.email.Hotmail;

public class Main {
    public static void main(String[] args) {
        Gmail gmail = new Gmail("smtp.google.com","user123","pass123");
        gmail.send("Hello","World");

        System.out.println();

        Hotmail hotmail = new Hotmail("smtp.microsoft.com","user123","pass123");
        hotmail.send("Hello","World");

    }
}