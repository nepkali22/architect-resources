package com.tecno.project.email;

import com.tecno.project.email.Email;

public class Gmail extends Email{

    public Gmail(String host, String username,String password){
        super(host,username,password);
    }

    protected String parseContent(String subject,String body){
        return "<html>\n"
            +"<head>\n"
                +"    <title>"+subject+"</title>\n"
            +"</head>\n"
            +"<body>\n    "
                +body+"\n"
            +"</body>\n"
            +"</html>\n";
    }

    protected void connect(){
        System.out.println("connecting to google --> "+this.host+"/"+this.username+":"+this.password);
    }

    protected void close(){
        System.out.println("closing connection to gmail");
    }

}