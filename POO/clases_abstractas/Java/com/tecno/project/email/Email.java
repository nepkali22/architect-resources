package com.tecno.project.email;

public abstract class Email{
    protected String host;
    protected String username;
    protected String password;

    public Email(String host,String username, String password){
        this.host = host;
        this.username = username;
        this.password = password;
    }

    public void send(String subject,String body){
        this.connect();
        System.out.println(this.parseContent(subject,body));
        this.close();
    }

    protected abstract String parseContent(String subject,String body);

    protected abstract void connect();

    protected abstract void close();
}