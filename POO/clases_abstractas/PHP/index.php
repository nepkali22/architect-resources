<?php

require "./vendor/autoload.php";

use Project\Email\Gmail;
use Project\Email\Hotmail;

$gmail = new Gmail("smtp.google.com","user123","pass123");
$gmail->send("Hello","World");

echo "\n";

$hotmail = new Hotmail("smtp.microsoft.com","user123","pass123");
$hotmail->send("Hello","World");
