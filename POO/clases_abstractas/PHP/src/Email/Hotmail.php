<?php
namespace Project\Email;

use Project\Email\Email;

class Hotmail extends Email{

    public function __construct(string $host, string $username,string $password){
        parent::__construct($host,$username,$password);
    }

    protected function parseContent(string $subject,string $body):string{
        return json_encode([
            "subject"=>$subject,
            "body"=>$body
        ],JSON_PRETTY_PRINT);
    }

    private function encodeConnection(string $connection):string{
        return base64_encode($connection);
    }

    protected function connect():void{
        $connectionEncoded = $this->encodeConnection($this->host."/".$this->username.":".$this->password);
        echo "connecting to hotmail -->".$connectionEncoded."\n";
    }

    protected function close():void{
        echo "closing connection to hotmail\n";
    }

}