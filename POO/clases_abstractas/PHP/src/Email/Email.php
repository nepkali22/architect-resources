<?php
namespace Project\Email;

abstract class Email{
    protected string $host;
    protected string $username;
    protected string $password;

    public function __construct(string $host,string $username, string $password){
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
    }

    public function send(string $subject,string $body):void{
        $this->connect();
        echo $this->parseContent($subject,$body)."\n";
        $this->close();
    }

    protected abstract function parseContent(string $subject,string $body):string;

    protected abstract function connect():void;

    protected abstract function close():void;
}