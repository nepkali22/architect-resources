#!/usr/bin/env bash

apt-get update
apt-get -y install software-properties-common
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install -y php7.4