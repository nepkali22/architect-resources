# REQUIREMENTS

- virtualbox latest version
- vagrant latest version

# INSTALATION

For create the virtual machine type:

```ssh
vagrant up
```

When the virtual machine is created for enter in the virtual machine and access to code type:

```ssh
vagrant ssh
cd code
```

First we need to compile the files into multiples *.class

```ssh
javac Main.java
or
javac Main2.java
```

And finally for execute the code type (in the two cases):

```ssh
java Main
or
java Main2
```

For exit to the virtual machine type **exit** command in the terminal

