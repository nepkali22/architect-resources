
FROM python:3

WORKDIR /home/project

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 9090

CMD [ "mkdocs", "serve" ]